"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NecromundaModel = exports.necromundaSchema = void 0;
var mongoose_1 = require("mongoose");
var player_1 = require("./player");
var necromundaSchema = new mongoose_1.Schema({
    players: [player_1.playerSchema]
});
exports.necromundaSchema = necromundaSchema;
var NecromundaModel = mongoose_1.model("Necromunda", necromundaSchema);
exports.NecromundaModel = NecromundaModel;
