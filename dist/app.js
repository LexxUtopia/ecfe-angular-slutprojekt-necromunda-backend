"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("cors"));
var mongoose_1 = __importDefault(require("mongoose"));
var routes_1 = __importDefault(require("./routes/routes"));
var app = express_1.default();
try {
    mongoose_1.default.connect('mongodb://necromunda:underhive@necromunda_db:27017/necromunda2?authSource=admin', { useNewUrlParser: true });
}
catch (error) {
    console.log("Error connecting");
}
app.use(cors_1.default());
app.use(express_1.default.json()); // for parsing application/json
app.use("/", routes_1.default);
app.listen(7878);
