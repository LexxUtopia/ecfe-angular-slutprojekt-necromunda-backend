"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var api_1 = require("../api");
var router = express_1.Router();
/*router.put('/api/player/:playerId/gang/:gangId/character/:characterId', api.updateCharacterById);
router.post('/api/player/:playerId/gang/:gangId/character', api.createCharacter);
router.put('/api/player/:playerId/gang/:gangId', api.updateGangById);
router.post('/api/player/:playerId/gang', api.createGang);
//router.post('/api/populate', api.populateNecromunda);
router.get('/api/player/:playerId', api.getPlayerById);*/
router.get('/api/player', api_1.api.getPlayers);
router.post('/api/player', api_1.api.addPlayer);
//router.get('/api', api.getNecromunda);
exports.default = router;
