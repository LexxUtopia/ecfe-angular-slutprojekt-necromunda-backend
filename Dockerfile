FROM node:14

WORKDIR /usr/local/src/app
COPY package.json .
COPY tsconfig.json .
RUN npm install

COPY . .
RUN npm run build

EXPOSE 7878

CMD npm run start