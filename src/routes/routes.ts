import { Router } from 'express';
import { api } from '../api';

const router = Router();

router.put('/api/player/:playerId/gang/:gangId/character/:characterId', api.updateCharacter);
router.get('/api/player/:playerId/gang/:gangId/character/:characterId', api.getCharacter);
router.get('/api/player/:playerId/gang/:gangId/character', api.getCharacters);
router.post('/api/player/:playerId/gang/:gangId/character', api.addCharacter);
router.put('/api/player/:playerId/gang/:gangId', api.updateGang);
router.get('/api/player/:playerId/gang/:gangId', api.getGang);
router.get('/api/player/:playerId/gang', api.getGangs);
router.post('/api/player/:playerId/gang', api.addGang)
router.get('/api/player/:playerId', api.getPlayer);
router.get('/api/player', api.getPlayers);
router.post('/api/player', api.addPlayer);

export default router;