import { Document, Model, model, Schema } from 'mongoose';
import { CharacterShape, characterSchema } from './character';

interface GangShape extends Document {
    uid: string;
    name: string;
    credits: number;
    meat: number;
    rating: number;
    reputation: number;
    wealth: number;
    characterIds: [string];
  }

const gangSchema = new Schema({
        uid: {
            type: String,
            required: true,
            unique: true
        },
        name: String,
        credits: Number,
        meat: Number,
        rating: Number,
        reputation: Number,
        wealth: Number,
        characterIds: {
            type: [String],
            required: false,
            default: new Array()
        }
    }
);

const GangModel: Model<GangShape> = model("Gang", gangSchema);

export { GangShape, gangSchema, GangModel };
