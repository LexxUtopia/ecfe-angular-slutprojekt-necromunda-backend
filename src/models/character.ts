import { Document, Model, model, Schema } from 'mongoose';

interface CharacterStatsShape extends Document {
    m: number;
    ws: number;
    bs: number;
    s: number;
    t: number;
    w: number;
    i: number;
    a: number;
    ld: number;
    cl: number;
    wil: number;
    int: number;
    xp: number;
}

const characterStatsSchema = new Schema({
    m: Number,
    ws: Number,
    bs: Number,
    s: Number,
    t: Number,
    w: Number,
    i: Number,
    a: Number,
    ld: Number,
    cl: Number,
    wil: Number,
    int: Number,
    xp: Number   
});

interface CharacterShape extends Document {
    uid: string;
    name: string;
    imgUrl?: string;
    stats: CharacterStatsShape;
}

const characterSchema = new Schema({
    uid: {
        type: String,
        required: false,
        unique: true
    },
    name: String,
    imgUrl: String,
    stats: characterStatsSchema
});

const CharacterModel: Model<CharacterShape> = model("Character", characterSchema);

export { CharacterStatsShape, characterStatsSchema, CharacterShape, characterSchema, CharacterModel };
